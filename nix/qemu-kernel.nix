{ writeScriptBin, pkgs, kernel, ... }:

writeScriptBin "qemu-kernel" ''
  #!${pkgs.bash}/bin/bash

  ${pkgs.qemu}/bin/qemu-system-i386 -cdrom ${kernel}/kernel.iso
''
