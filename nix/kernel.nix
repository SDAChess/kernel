{ pkgs, ... }:

pkgs.pkgsCross.i686-embedded.stdenv.mkDerivation {
  pname = "kernel";
  version = "0.0.1";
  src = ../.;
  nativeBuildInputs = with pkgs.buildPackages; [ grub2 xorriso cmake ];

  installPhase = ''
    runHook preInstall
    mkdir -p $out/
    cp kernel.iso $out/kernel.iso
    runHook postInstall
  '';
}
