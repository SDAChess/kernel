{ pkgs, ... }:

pkgs.pkgsCross.i686-embedded.mkShell {
  nativeBuildInputs = with pkgs.buildPackages; [ grub2 xorriso qemu cmake ];
}
