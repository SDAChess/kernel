{
  description = "A learning kernel written in C";

  inputs = {
    nixpkgs = { url = "github:NixOS/nixpkgs/nixpkgs-unstable"; };
    flake-utils = { url = "github:numtide/flake-utils"; };
  };

  outputs = { nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
      in
      rec {
        packages = rec {
          qemu-kernel = pkgs.callPackage ./nix/qemu-kernel.nix { kernel = packages.kernel; };
          kernel = import ./nix/kernel.nix { inherit pkgs; };
          default = kernel;
        };
        devShells.default = import ./nix/shell.nix { inherit pkgs; };
      });
}
