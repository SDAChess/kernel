#ifndef LIBK_STRING_H_
#define LIBK_STRING_H_

#include <stddef.h>

size_t strlen(const char *data);

#endif // LIBK_STRING_H_
