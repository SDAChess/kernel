#include "libk/string.h"

size_t strlen(const char *data)
{
    size_t size = 0;

    for (size_t i = 0; data[i] != 0; i++)
    {
        size += 1;
    }
    return size;
}
