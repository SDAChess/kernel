#ifndef TTY_H_
#define TTY_H_

#include <stddef.h>

void tty_init(void);
void tty_putchar(char c);
void tty_write(const char *buffer, size_t size);
void tty_puts(const char *string);

#endif // TTY_H_
