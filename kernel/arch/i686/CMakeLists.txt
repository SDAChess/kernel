target_link_options(
  kernel
  PRIVATE
  -T
  ${CMAKE_CURRENT_SOURCE_DIR}/linker.ld
  -ffreestanding
  -O2
  -nostdlib
  -lgcc)

target_sources(kernel PRIVATE boot.s tty.c)

add_custom_command(
  OUTPUT kernel.iso
  COMMAND ${CMAKE_COMMAND} -E make_directory isodir/boot/grub
  COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:kernel> isodir/boot/kernel.elf
  COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/grub.cfg
          isodir/boot/grub/grub.cfg
  COMMAND grub-mkrescue -o $<TARGET_FILE_DIR:kernel>/kernel.iso isodir
  DEPENDS $<TARGET_FILE:kernel>
  COMMENT "Creating iso from kernel elf file"
  VERBATIM)

add_custom_target(iso ALL DEPENDS kernel.iso)
