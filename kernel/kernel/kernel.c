#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "kernel/tty.h"

void kernel_main(void)
{
    tty_init();
    tty_puts("Hello, World!");
}
