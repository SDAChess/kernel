function(check_target)
  execute_process(COMMAND ${CMAKE_C_COMPILER} "-dumpmachine"
                  OUTPUT_VARIABLE C_COMPILER_ARCH)
  if(C_COMPILER_ARCH MATCHES "${ARCH}.*")
    message(STATUS "C compiler architecture detected: ${C_COMPILER_ARCH}")
  else()
    message(FATAL_ERROR "C compiler doesn't support architecture ${ARCH}")
  endif()
endfunction()
